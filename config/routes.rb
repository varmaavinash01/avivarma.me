Rails.application.routes.draw do
  resources :quotes
  resources :projects
  root 'statics#index'
  get '/resume' => 'statics#resume'
  resources :random_thoughts
  resources :books

  get '/webhook/whatsapp' => 'webhooks#whatsapp'
  post '/webhook/whatsapp' => 'webhooks#whatsapp'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
