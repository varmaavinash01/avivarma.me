class RandomThoughtsController < ApplicationController
  before_action :set_random_thought, only: [:show, :edit, :update, :destroy]
  before_action :authenticate, :except => [:index, :show]

  # GET /random_thoughts
  # GET /random_thoughts.json
  def index
    if params[:view] == 'admin'
      @random_thoughts = RandomThought.order_by(:rating => 'desc')
    else
      @random_thoughts = RandomThought.where(:visible => 1)
    end
  end

  # GET /random_thoughts/1
  # GET /random_thoughts/1.json
  def show
  end

  # GET /random_thoughts/new
  def new
    @random_thought = RandomThought.new
  end

  # GET /random_thoughts/1/edit
  def edit
  end

  # POST /random_thoughts
  # POST /random_thoughts.json
  def create
    @random_thought = RandomThought.new(random_thought_params)

    respond_to do |format|
      if @random_thought.save
        format.html { redirect_to @random_thought, notice: 'Random thought was successfully created.' }
        format.json { render :show, status: :created, location: @random_thought }
      else
        format.html { render :new }
        format.json { render json: @random_thought.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /random_thoughts/1
  # PATCH/PUT /random_thoughts/1.json
  def update
    respond_to do |format|
      if @random_thought.update(random_thought_params)
        format.html { redirect_to @random_thought, notice: 'Random thought was successfully updated.' }
        format.json { render :show, status: :ok, location: @random_thought }
      else
        format.html { render :edit }
        format.json { render json: @random_thought.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /random_thoughts/1
  # DELETE /random_thoughts/1.json
  def destroy
    @random_thought.destroy
    respond_to do |format|
      format.html { redirect_to random_thoughts_url, notice: 'Random thought was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_random_thought
      @random_thought = RandomThought.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def random_thought_params
      params[:random_thought][:tags] = params[:random_thought][:tags].split(',')
      params.require(:random_thought).permit(:title, :short_description, :long_description, :rating, :visible, :tags => [])
    end
end
