class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  protected
  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == ENV['avivarma_admin_name'] && password == ENV['avivarma_admin_pass']
    end
  end

end
