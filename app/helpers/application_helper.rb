module ApplicationHelper
  def admin?
    params[:view] == 'admin'
  end

  def title_name
    case params[:controller]
      when 'statics' then 'Profile'
      when 'projects' then 'Side Projects'
      when 'books' then 'Reading List'
      when 'quotes' then 'Favourite Quotes'
      when 'random_thoughts' then 'Random Thoughts'
      else
        'Profile'
    end
  end

  def add_href_to_link(str='')
    str.gsub!( %r{https://[^\s<]+} ) do |url|
      if url[/(?:png|jpe?g|gif|svg)$/]
        "<img src='#{url}' />"
      else
        "<a target='_blank' href='#{url}'>#{url}</a>"
      end
    end
    str
  end
end

