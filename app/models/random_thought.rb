class RandomThought
  include Mongoid::Document
  include Mongoid::Timestamps
  field :title, type: String
  field :short_description, type: String
  field :long_description, type: String
  field :rating, type:Integer
  field :visible, type:Integer
  field :tags, type: Array
end
