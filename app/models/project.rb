class Project
  include Mongoid::Document
  include Mongoid::Timestamps
  field :title, type: String
  field :short_description, type: String
  field :long_description, type: String
  field :why, type: String
  field :how, type: String
  field :what_is_next, type: String
  field :service_url, type: String
  field :cover_image, type: String
  field :video_demo, type: String
  field :rating, type:Integer
  field :technologies_used, type: Array, default: []
  field :screenshot_links, type: Array, default: []
end
