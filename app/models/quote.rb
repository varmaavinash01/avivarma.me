class Quote
  include Mongoid::Document
  include Mongoid::Timestamps
  field :quote, type: String
  field :said_by, type: String
  field :author_mugshot, type: String
  field :rating, type:Integer
  field :tags, type: Array, default: []
end
