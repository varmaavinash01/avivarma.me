class Book
  include Mongoid::Document
  include Mongoid::Timestamps
  field :isbn, type: String
  field :status, type: String
  field :title, type: String
  field :author, type: String
  field :short_description, type: String
  field :opinion, type: String
  field :rating, type:Integer
  field :quotes, type: Array, default: []
  field :images, type: Array, default: []
  field :tags, type: Array, default: []
end
