json.extract! random_thought, :id, :title, :short_description, :long_description, :tags, :created_at, :updated_at
json.url random_thought_url(random_thought, format: :json)
