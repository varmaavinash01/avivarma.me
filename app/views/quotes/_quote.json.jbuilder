json.extract! quote, :id, :quote, :said_by, :author_mugshot, :tags, :created_at, :updated_at
json.url quote_url(quote, format: :json)
