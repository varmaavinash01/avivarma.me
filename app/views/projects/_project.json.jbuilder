json.extract! project, :id, :title, :short_description, :long_description, :why, :how, :what_is_next, :service_url, :cover_image, :video_demo, :technologies_used, :screenshot_links, :created_at, :updated_at
json.url project_url(project, format: :json)
