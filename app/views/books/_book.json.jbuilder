json.extract! book, :id, :isbn, :status, :title, :author, :short_description, :opinion, :quotes, :images, :tags, :created_at, :updated_at
json.url book_url(book, format: :json)
