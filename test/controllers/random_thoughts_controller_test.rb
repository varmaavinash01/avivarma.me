require 'test_helper'

class RandomThoughtsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @random_thought = random_thoughts(:one)
  end

  test "should get index" do
    get random_thoughts_url
    assert_response :success
  end

  test "should get new" do
    get new_random_thought_url
    assert_response :success
  end

  test "should create random_thought" do
    assert_difference('RandomThought.count') do
      post random_thoughts_url, params: { random_thought: { long_description: @random_thought.long_description, short_description: @random_thought.short_description, tags: @random_thought.tags, title: @random_thought.title } }
    end

    assert_redirected_to random_thought_url(RandomThought.last)
  end

  test "should show random_thought" do
    get random_thought_url(@random_thought)
    assert_response :success
  end

  test "should get edit" do
    get edit_random_thought_url(@random_thought)
    assert_response :success
  end

  test "should update random_thought" do
    patch random_thought_url(@random_thought), params: { random_thought: { long_description: @random_thought.long_description, short_description: @random_thought.short_description, tags: @random_thought.tags, title: @random_thought.title } }
    assert_redirected_to random_thought_url(@random_thought)
  end

  test "should destroy random_thought" do
    assert_difference('RandomThought.count', -1) do
      delete random_thought_url(@random_thought)
    end

    assert_redirected_to random_thoughts_url
  end
end
